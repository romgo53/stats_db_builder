import pymongo
import csv
import json

playerdt_dict = {
     1: "key", 2: "Name", 3: "weight",
    4: "height", 5: "born" , 6:"birth_city" , 7: "birth_state" , 8: "collage"
}

csv_folder_prefix = "Players"
json_folder_prefix = "file"
file_name = "Players"


def _append_items_list(list_to_check_values):
    list_to_append = []
    for value in list_to_check_values:
        current_index = list_to_check_values.index(value)
        list_to_append.append({
            playerdt_dict[current_index]: value
        })
    return list_to_append


def _save_in_json(dict):
    # print(dict)
    # Writing JSON data
    with open(json_folder_prefix + file_name + '.json', "w") as file:
        json.dump(dict, file)


new_dict = {}
with open(csv_folder_prefix + '.csv', 'r') as csvFile:
    reader = csv.reader(csvFile)
    for row in reader:
        if "id" not in row[0]:
            new_dict[row[0]] = (playerdt_dict[1] + ": " + row[1], playerdt_dict[2] + ": " + row[2], playerdt_dict[3] + ": " + row[3], playerdt_dict[4] + ": " + row[4],
                                    playerdt_dict[5] + ": " + row[5], playerdt_dict[6] + ": " + row[6], playerdt_dict[7] + ": " + row[7], playerdt_dict[8] + ": " + row[8])


def map_fn(args):
     index, value = args



# new_dict[row[0]] = list(map(map_fn, list(enumerate(all_names))))

csvFile.close()
_save_in_json(new_dict)

# .... client = pymongo.MongoClient("mongodb://localhost:27017/") #

# PlayersDB = client["PlayerDB"] #
# PlayersCol = PlayersDB["Players"] #

# mydict = {"key": "", "name": "LeBron James", "height": "203", "weight": "113", "collage": "", "born": "1984", "birth_city": "Akron", "birth_state": "Ohio"} #


# for x in PlayersCol.find({}, {"height": "203"}): #
# print(x)  #
