import os
import json
import pathlib
from pymongo import MongoClient
#setup the database
os.environ['MONGODB_DATABASE1']='player_stat_db2'
MONGODB_HOSTNAME = os.environ['MONGODB_HOSTNAME1']
MONGODB_DATABASE = os.environ['MONGODB_DATABASE1']

MONGO_URI = 'mongodb//' + MONGODB_HOSTNAME + ':27017/' + MONGODB_DATABASE

myclient = MongoClient(MONGODB_HOSTNAME, 27017)
my_db = myclient[MONGODB_DATABASE]




def testing():
    print("All is OK!!!")


def create_collections():
    directory = 'DB_Creator/'
    file_name = 'Stat_file.json'

    with open(file_name) as f:
        json_file_data = json.load(f)
        file_name_no_postfix = file_name.split(".")[0]
        collection = my_db[file_name_no_postfix]
        # collection.insert_many(file_data)
        document = {}
        players_names = []

        for f in json_file_data:
            count = 0
            document = {}
            if json_file_data[str(f)]["Player"] in players_names:
                f = int(f)
                f += 1
                f = str(f)
            else:
                players_names.append(data[f]["Player"])
            players_names = json_file_data[str(f)]["Player"]
            for t in json_file_data:
                if json_file_data[str(t)]["Player"] == name:
                    document[data[str(t)]["Year"]] = data[str(t)]
                    count = count + 1
            collection.insert_one(document)

#check if the database exist
def check_db(db_name):
    db_list = myclient.list_database_names()
    if db_name in dblist:
        print("The database exists.")


def check_collection(collection_name):
    collist = my_db.list_collection_names()
    if collection_name in collist:
        print("The collection exists.")


def get_all_items_from_collection(collection_name):
    db_list = []
    col = my_db[collection_name]
    for entry in col.find():
        db_list.append(entry)

    return db_list


def close():
    myclient.close()

create_collections()
