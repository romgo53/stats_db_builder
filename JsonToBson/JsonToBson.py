import json
import bson


def from_json(self, json_data):
    """Convert from JSON."""
    mongo_data = json.loads(json_data, object_hook = generate_object_hook(self._document)
    )
    exclude = [
        name for (name, fld) in self._document._fields.items() if any([
            getattr(fld, "exclude_from_json", None),
            getattr(fld, "exclude_json", None)
        ])
    ]
    for item in mongo_data:
        for exc in exclude:
            item.pop(exc, None)
    return [
        self._document._from_son(bson.SON(data)) for data in mongo_data
    ]